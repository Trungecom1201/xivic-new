import PageManager from './page-manager';
import $ from "jquery";

export default class Home extends PageManager {
    loaded(next) {
        super.loaded(next);
        //this.showDescription();
        this.slideRun();
        this.popupVideo();
    }

    slideRun(){
        $('.works-list').slick({
            slidesToShow:1,
            slidesToScroll:1,
            dots:true
        });
        $('.reviews-list').slick({
            slidesToShow:1,
            slidesToScroll:1,
            dots:true,
            autoplay: false,
            autoplaySpeed: 2000,
            speed: 300
        });
        $('.review img').click(function(){
            if($(this).hasClass('active')){
                $(this).removeClass('active');
                $('.reviews-list').slick('slickPause');
            }else{
                $(this).addClass('active');
                $('.reviews-list').slick('slickPlay');
            }
        });
        setTimeout(function(){
            $(window).resize(function(){
                var width = $(window).width();
                if(width < 768){
                    $(".instagram .instagram-block .content-instagram").remove();
                    $(".instagram .instagram-block .images-instagram .sub-link").remove();
                }
            });

            var width = $(window).width();
            if(width < 768){
                $(".instagram .instagram-block .content-instagram").remove();
                $(".instagram .instagram-block .images-instagram .sub-link").remove();
            }

            $('.instagram-block').slick({
                rows: 1,
                slidesToShow:3,
                slidesToScroll:1,
                dots:false,
                speed: 500,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            rows: 1,
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            rows: 2,
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            arrows: false
                        }
                    }
                ]
            });

        }, 1000);

    }
    popupVideo(){
        $('.play-video').on('click', function(ev) {
            playVideo();
        });
        $('.close-video').on('click', function(){
            pauseVideo();
        });
        $('.mask').on('click',function(e) {
            pauseVideo();
        });
        function playVideo() {
            $('.video-popup,.mask').fadeIn(400);
            $('#video')[0].play();
        }
        function pauseVideo() {
            $('.video-popup,.mask').fadeOut(400);
            $('#video').trigger('pause');
        }
    }
}
