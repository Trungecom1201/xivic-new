import $ from 'jquery';

export default function(){

    toggleTheGoods();
    mrCategory();
    checkCart();
    blogAPIHomePage();
    blogAPIProduct();
    blogAPIMenu();
    clickMenuMobile();
    productJs();
    openVideo();
    timeDay();
}

function toggleTheGoods(){
    $('.benefits-info .drop').click(function () {
        $(this).parents('.benefits-info').toggleClass('active');
    });

    var itemMenu = $("#header .navPages .navPages-item:first-child .navPage-subMenu-action");
    itemMenu.click(function(){
        var link = $(this).attr("href");
        window.location.replace(link);
    })
}

function mrCategory(){
    $(".template-pages-cart .cart-totals .cart-total").each(function(){
        var text = $(this).find(".cart-total-label strong").text();
        if(text == "Tax:"){
            $(this).hide();
        }
    });

    $(document).ready(function(){
        var option = $(".form-field.quantity .option");
        option.each(function(){
            var a = $(this).find(".form-radio").attr("checked"),
                b = $(this).find(".form-radio");
            if(a){
                var c = b.parents(".option").find(".form-option-variant").text(),
                    priceSave = $(".price-item-save .item-value-save");

                priceSave.each(function () {
                    var data = $(this).attr("dataSave");
                    if(data == c){
                        $(this).addClass("active");
                    }
                })
            }

            var formClick = $(this).find(".form-option");
            formClick.click(function(){
                $(".form-field.quantity .option").removeClass("active");
                $(this).parents(".option").addClass("active");
                $(".quantity .option .form-radio").removeAttr("checked");
                $(this).parents(".option").find(".form-radio").checked = true;
                var checkOpen = $(".form-field.deliver .form-label");
                if(checkOpen.hasClass("active")){
                    var textVariant = $(this).find(".form-option-variant").text();
                    priceSave = $(".price-item-save .item-value-save");

                    priceSave.each(function () {
                        var data = $(this).attr("dataSave");
                        if(data == textVariant){
                            $(".price-item-save .item-value-save").removeClass("active");
                            $(this).addClass("active");
                            $(".productView-price .real-price").show();
                        }
                    })
                } else{
                    var text = $(this).find(".form-option-variant").text();
                    if(text == "14"){
                        //var subValue = $(".price-item-save .item-value-save");
                        //subValue.each(function(){
                        //    var check = $(this).attr("datasave");
                        //    if(check == "onetime14"){
                        $(".price-item-save .item-value-save").removeClass("active");
                        //$(this).addClass("active");
                        $(".productView-price .real-price").hide();
                        //    }
                        //})
                    } else if(text == "28"){
                        var subValue = $(".price-item-save .item-value-save");
                        subValue.each(function(){
                            var check = $(this).attr("datasave");
                            if(check == "onetime28"){
                                $(".price-item-save .item-value-save").removeClass("active");
                                $(this).addClass("active");
                                $(".productView-price .real-price").show();
                            }
                        })
                    } else{
                        var subValue = $(".price-item-save .item-value-save");
                        subValue.each(function(){
                            var check = $(this).attr("datasave");
                            if(check == "onetime42"){
                                $(".price-item-save .item-value-save").removeClass("active");
                                $(this).addClass("active");
                                $(".productView-price .real-price").show();
                            }
                        })
                    }
                }

            })
        });

        $(document).on("click",".one-time .one-time-box", function(){
            var option = $(".form-field.quantity .option");
            option.each(function(){
                var checkedItem = $(this).find(".form-radio").attr("checked"),
                    $this = $(this);
                if(checkedItem){
                    var text = $(this).find(".form-option-variant").text();
                    if(text == "14"){
                        var subValue = $(".price-item-save .item-value-save");
                        subValue.each(function(){
                            var check = $(this).attr("datasave");
                            if(check == "onetime14"){
                                $(".price-item-save .item-value-save").removeClass("active");
                                $(this).addClass("active");
                                $(".productView-price .real-price").hide();
                            }
                        })
                    } else if(text == "28"){
                        var subValue = $(".price-item-save .item-value-save");
                        subValue.each(function(){
                            var check = $(this).attr("datasave");
                            if(check == "onetime28"){
                                $(".price-item-save .item-value-save").removeClass("active");
                                $(this).addClass("active");
                                $(".productView-price .real-price").show();
                            }
                        })
                    } else{
                        var subValue = $(".price-item-save .item-value-save");
                        subValue.each(function(){
                            var check = $(this).attr("datasave");
                            if(check == "onetime42"){
                                $(".price-item-save .item-value-save").removeClass("active");
                                $(this).addClass("active");
                                $(".productView-price .real-price").show();
                            }
                        })
                    }
                }
                else if($this.hasClass("active")){
                    var checkActive = $this.find(".form-option-variant").text();
                    if(checkActive == "14"){
                        //var subValue = $(".price-item-save .item-value-save");
                        //subValue.each(function(){
                        //    var check = $(this).attr("datasave");
                        //    if(check == "onetime14"){
                        $(".price-item-save .item-value-save").removeClass("active");
                        //$(this).removeClass("active");
                        $(".productView-price .real-price").hide();
                        //    }
                        //})
                    } else if(checkActive == "28"){
                        var subValue = $(".price-item-save .item-value-save");
                        subValue.each(function(){
                            var check = $(this).attr("datasave");
                            if(check == "onetime28"){
                                $(".price-item-save .item-value-save").removeClass("active");
                                $(this).addClass("active");
                                $(".productView-price .real-price").show();
                            }
                        })
                    } else{
                        var subValue = $(".price-item-save .item-value-save");
                        subValue.each(function(){
                            var check = $(this).attr("datasave");
                            if(check == "onetime42"){
                                $(".price-item-save .item-value-save").removeClass("active");
                                $(this).addClass("active");
                                $(".productView-price .real-price").show();
                            }
                        })
                    }
                }
            });
        });

        $(document).on("click",".form-field.deliver .form-label .box-deliver", function(){
            var option = $(".form-field.quantity .option");
            option.each(function(){
                if($(this).hasClass("active")){
                    var text = $(this).find(".form-option-variant").text(),
                        checkText = $(".price-item-save .item-value-save");
                    checkText.each(function(){
                        var a = $(this).attr("datasave");
                        if(a == text){
                            $(".price-item-save .item-value-save").removeClass("active");
                            $(this).addClass("active");
                            $(".productView-price .real-price").show();
                        }
                    })
                }
            })
        });
    });
}

function checkCart(){
    $(document).ready(function() {
        $(".content-cart-list .boxcart-item").each(function () {
            var text = $(this).find(".cart-item-quantity .deliver-every").text(),
                $this = $(this);
            if (text == "One Time Only") {
                var datanumber = $(this).find(".number-qty .item-save.quantity").attr("datanumber"),
                    textSave = $(this).find(".cart-item-total .item-save-total .subItem-save");
                textSave.each(function () {
                    if (datanumber == "14") {
                        $this.find(".cart-item-total .item-save-total .subItem-save.data-14").addClass("active");
                    } else if (datanumber == "28") {
                        $this.find(".cart-item-total .item-save-total .subItem-save.data-28").addClass("active");
                    } else {
                        $this.find(".cart-item-total .item-save-total .subItem-save.data-42").addClass("active");
                    }
                });
            } else {
                var datanumber = $(this).find(".number-qty .item-save.quantity").attr("datanumber"),
                    textSave = $(this).find(".cart-item-total .item-save-total .subItem-save");
                textSave.each(function () {
                    var a = $(this).attr("datasave");
                    if (a == datanumber) {
                        $(this).addClass("active");
                    }
                })
            }
        });
    });
}

function clickMenuMobile(){
    $(window).resize(function(){
        const width = $(window).width();
        if(width > 992 ){
            $(".navPages .navPages-item:first-child .navPages-action").click(function(){
                var link = $(this).attr("href");
                window.location.replace(link);
            });
        } else{
            $(".navPages .navPages-item:first-child .navPages-action").click(function(){
               return false;
            });

            $(".mobileMenu-toggle").click(function(){
                $(".header .navPages .navPages-item").removeClass("open");
                $(".header .navPages .navPages-item:first-child").addClass("open");
                $(".header .navPages .navPages-item >.navPages-action").removeClass("active");
                $(".header .navPages .navPages-item:first-child >.navPages-action").addClass("active");
            });

            $('.natural-list').slick({
                slidesToShow:1,
                slidesToScroll:1,
                arrows:true,
                dots:true
            });
            $('.our-list').slick({
                slidesToShow:1,
                slidesToScroll:1,
                arrows:false,
                infinite:false
            });
        }
    });

    $(document).ready(function(){
        const width = $(window).width();
        if(width > 992 ){
            $(".navPages .navPages-item:first-child .navPages-action").click(function(){
                var link = $(this).attr("href");
                window.location.replace(link);
            });

        } else{
            $(".navPages .navPages-item:first-child .navPages-action").click(function(){
                return false;
            });

            $(".mobileMenu-toggle").click(function(){
                $(".header .navPages .navPages-item").removeClass("open");
                $(".header .navPages .navPages-item:first-child").addClass("open");
                $(".header .navPages .navPages-item >.navPages-action").removeClass("active");
                $(".header .navPages .navPages-item:first-child >.navPages-action").addClass("active");
            });

            $('.natural-list').slick({
                slidesToShow:1,
                slidesToScroll:1,
                arrows:true,
                dots:true
            });
            $('.our-list').slick({
                slidesToShow:1,
                slidesToScroll:1,
                arrows:false,
                infinite:false
            });
        }

    });
}


function openVideo(){
    //var vid = document.getElementById("myVideo");

    $(".science-content-videos .openVideo").click(function(){
        $(".science-content-videos .modal-videos").addClass("is-open");
        $(".science-content-videos .mask-videos").addClass("is-active");
        //vid.play();
    });

    $(".science-content-videos .closeVideo").click(function(){
        $(".science-content-videos .modal-videos").removeClass("is-open");
        $(".science-content-videos .mask-videos").removeClass("is-active");
        //vid.pause();
    });

    $(".science-content-videos .mask-videos").click(function () {
        $(this).removeClass("is-active");
        $(".science-content-videos .modal-videos").removeClass("is-open");
        //vid.pause();
    });
}


function blogAPIHomePage(){
    //Blog API on homepage
    $(document).ready(function(){
        $.ajax({
            url: "https://muniqwp.dev-directory.com/wp-json/api/homepage-posts",
            jsonp: "_jsonp",
            method: 'GET',
            dataType: 'jsonp',

            success: function(data) {
                var $ambassador = '<div class="item-ambassador itemBlog-homepage">' +
                    '<div class="post-img"><img src="' + data.ambassador.featured_image + '"></div>' +
                    '<h4>Our Ambassadors</h4>' +
                    '<a class="link-blog" target="_blank" href="' + data.ambassador.link + '">' + data.ambassador.title + '</a>' +
                    '<a class="see-all" target="_blank" href="' +  data.ambassador.see_all_url + '">See all' +
                    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 492.004 492.004" style="enable-background:new 0 0 492.004 492.004;width: 11px;height: 11px;" xml:space="preserve"> <path d="M382.678,226.804L163.73,7.86C158.666,2.792,151.906,0,144.698,0s-13.968,2.792-19.032,7.86l-16.124,16.12    c-10.492,10.504-10.492,27.576,0,38.064L293.398,245.9l-184.06,184.06c-5.064,5.068-7.86,11.824-7.86,19.028    c0,7.212,2.796,13.968,7.86,19.04l16.124,16.116c5.068,5.068,11.824,7.86,19.032,7.86s13.968-2.792,19.032-7.86L382.678,265    c5.076-5.084,7.864-11.872,7.848-19.088C390.542,238.668,387.754,231.884,382.678,226.804z" style="&#10;fill: #004936;&#10;"/></svg>' +
                    '</a></div>';
                var $article =  '<div class="item-ambassador itemBlog-homepage">' +
                    '<div class="post-img"><img src="' + data.article.featured_image + '"></div>' +
                    '<h4>Recent Articles</h4>' +
                    '<a class="link-blog" target="_blank" href="' + data.article.link + '">' + data.article.title + '</a>' +
                    '<a class="see-all" target="_blank" href="' +  data.article.see_all_url + '">See all' +
                    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 492.004 492.004" style="enable-background:new 0 0 492.004 492.004;width: 11px;height: 11px;" xml:space="preserve"> <path d="M382.678,226.804L163.73,7.86C158.666,2.792,151.906,0,144.698,0s-13.968,2.792-19.032,7.86l-16.124,16.12    c-10.492,10.504-10.492,27.576,0,38.064L293.398,245.9l-184.06,184.06c-5.064,5.068-7.86,11.824-7.86,19.028    c0,7.212,2.796,13.968,7.86,19.04l16.124,16.116c5.068,5.068,11.824,7.86,19.032,7.86s13.968-2.792,19.032-7.86L382.678,265    c5.076-5.084,7.864-11.872,7.848-19.088C390.542,238.668,387.754,231.884,382.678,226.804z" style="&#10;fill: #004936;&#10;"/></svg>' +
                    '</a></div>';
                var $recipe =  '<div class="item-ambassador itemBlog-homepage">' +
                    '<div class="post-img"><img src="' + data.recipe.featured_image + '"></div>' +
                    '<h4>Delicious Recipes</h4>' +
                    '<a class="link-blog" target="_blank" href="' + data.recipe.link + '">' + data.recipe.title + '</a>' +
                    '<a class="see-all" target="_blank" href="' +  data.recipe.see_all_url + '">See all' +
                    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 492.004 492.004" style="enable-background:new 0 0 492.004 492.004;width: 11px;height: 11px;" xml:space="preserve"> <path d="M382.678,226.804L163.73,7.86C158.666,2.792,151.906,0,144.698,0s-13.968,2.792-19.032,7.86l-16.124,16.12    c-10.492,10.504-10.492,27.576,0,38.064L293.398,245.9l-184.06,184.06c-5.064,5.068-7.86,11.824-7.86,19.028    c0,7.212,2.796,13.968,7.86,19.04l16.124,16.116c5.068,5.068,11.824,7.86,19.032,7.86s13.968-2.792,19.032-7.86L382.678,265    c5.076-5.084,7.864-11.872,7.848-19.088C390.542,238.668,387.754,231.884,382.678,226.804z" style="&#10;fill: #004936;&#10;"/></svg>' +
                    '</a></div>';
                var $emptyAmbassador = data.ambassador.id;
                var $emptyArticle = data.article.id;
                var $emptyRecipe = data.recipe.id;
                if ($emptyAmbassador){
                    $('#blog-discover-muniq .left-content').append($ambassador);
                }
                if ($emptyArticle){
                    $('#blog-discover-muniq .right-content').append($article);
                }
                if ($emptyRecipe){
                    $('#blog-discover-muniq .right-content').append($recipe);
                }
            },
        });

        $.ajax({
            type: "GET",
            dataType: "jsonp",
            cache: false,
            url: "https://api.instagram.com/v1/users/self/media/recent/?access_token=23546286605.1677ed0.60db206bef324b9ca06f713f3face3a6",
            success: function(response) {
                var i = 0;
                for (i ; i < 20; i++) {
                    var $itemBox_insta =
                        '<div class="itemBox-instagram">' +
                        '<div class="images-instagram"><a target="_blank" href="' + response.data[i].link + '"><img src="' + response.data[i].images.standard_resolution.url + '"></a>' +
                        '<a class="sub-link" target="_blank" href="' + response.data[i].link + '"><i class="fa fa-instagram"></i></a>' +
                        '</div>' +
                        '<div class="content-instagram">' +
                        '<div class="avatar-user"><a class="link-instagram" target="_blank" href="https://www.instagram.com/' + response.data[i].user.username + '"><img src="' + response.data[i].user.profile_picture + '"></a></div>' +
                        '<div class="infor-instagram"><a class="link-instagram" target="_blank" href="https://www.instagram.com/' + response.data[i].user.username + '">' + response.data[i].user.full_name + '</a>' +
                        '<span class="username">@' + response.data[i].user.username + '</span>' +
                        '</div>' +
                        '</div>' +
                        '</div>';

                    $('.instagram-block').append($itemBox_insta);
                };
            },
        });
    });
}

function blogAPIProduct(){
    $(document).ready(function(){
        //Blog API on product
        $.ajax({
            url: "https://muniqwp.dev-directory.com/wp-json/api/latest-posts?category=ambassadors&limit=3",
            jsonp: "_jsonp",
            method: 'GET',
            dataType: 'jsonp',

            success: function(data) {
                var i = 1;
                for (i ; i < 4; i++) {
                    var $item_blog = '<div class="item-blog">' +
                        '<div class="post-img"><a target="_blank" href="' + data.items[i].link + '">' +
                        '<img src="' + data.items[i].featured_image + '"></div>' +
                        '<h2>' + data.items[i].title + '</h2><a></div>';
                    $('#main-meet-our').append($item_blog);
                };

                var $seeAll = '<a target="_blank" href="' +  data.see_all_url + '">See all <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 492.004 492.004" style="enable-background:new 0 0 492.004 492.004;width: 11px;height: 11px;" xml:space="preserve"> <path d="M382.678,226.804L163.73,7.86C158.666,2.792,151.906,0,144.698,0s-13.968,2.792-19.032,7.86l-16.124,16.12    c-10.492,10.504-10.492,27.576,0,38.064L293.398,245.9l-184.06,184.06c-5.064,5.068-7.86,11.824-7.86,19.028    c0,7.212,2.796,13.968,7.86,19.04l16.124,16.116c5.068,5.068,11.824,7.86,19.032,7.86s13.968-2.792,19.032-7.86L382.678,265    c5.076-5.084,7.864-11.872,7.848-19.088C390.542,238.668,387.754,231.884,382.678,226.804z" style="&#10;    fill: #004936;&#10;"/></svg></a>';
                $('#meet-our-ambassadors .link-meet-our').append($seeAll);
            },
        });
    });
}

function blogAPIMenu(){
    $(document).ready(function(){
        // Blog Menu
        $.ajax({
            url: "https://muniqwp.dev-directory.com/wp-json/api/latest-posts?limit=3",
            jsonp: "_jsonp",
            method: 'GET',
            dataType: 'jsonp',

            success: function(data) {
                var i = 1;
                for (i ; i < 4; i++) {
                    var $item_blog = '<div class="item-blog">' +
                        '<div class="post-img"><img src="' + data.items[i].featured_image + '"></div>' +
                        '<h2 class="title"><a target="_blank" href="' + data.items[i].link + '">' + data.items[i].title + '</a></h2></div>';
                    $('#content-blogMenu .col-blogMenu').append($item_blog);
                };

                var $seeAll = 'Blog<a target="_blank" href="' +  data.see_all_url + '">See all <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 492.004 492.004" style="enable-background:new 0 0 492.004 492.004;width: 11px;height: 11px;" xml:space="preserve"> <path d="M382.678,226.804L163.73,7.86C158.666,2.792,151.906,0,144.698,0s-13.968,2.792-19.032,7.86l-16.124,16.12    c-10.492,10.504-10.492,27.576,0,38.064L293.398,245.9l-184.06,184.06c-5.064,5.068-7.86,11.824-7.86,19.028    c0,7.212,2.796,13.968,7.86,19.04l16.124,16.116c5.068,5.068,11.824,7.86,19.032,7.86s13.968-2.792,19.032-7.86L382.678,265    c5.076-5.084,7.864-11.872,7.848-19.088C390.542,238.668,387.754,231.884,382.678,226.804z" style="&#10;    fill: #004936;&#10;"/></svg></a>';
                $('#content-blogMenu .col-blogMenu .heading-blog').append($seeAll);

                var $seeMobile = '<a target="_blank" href="' +  data.see_all_url + '">See articles <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="4px" height="7px" viewBox="0 0 4 7" version="1.1" <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square"> <g id="Muniq-menu-mobile-3" transform="translate(-269.000000, -575.000000)" stroke="#004936"> <g id="Group-4-Copy-6" transform="translate(269.500000, 578.500000) rotate(-45.000000) translate(-269.500000, -578.500000) translate(267.000000, 576.000000)"> <line x1="0.5" y1="4.5" x2="4.5" y2="4.5" id="Line-10"/> <line x1="4.5" y1="4.5" x2="4.5" y2="0.5" id="Line"/> </g> </g> </g> </svg></a>';
                $('#content-blogMenu-mobile .col-blog-mobile .content-blog').append($seeMobile);
            }
        });

        // Recipes Menu
        $.ajax({
            url: "https://muniqwp.dev-directory.com/wp-json/api/latest-posts?category=recipes&limit=3",
            jsonp: "_jsonp",
            method: 'GET',
            dataType: 'jsonp',

            success: function(data) {
                var i = 1;
                for (i ; i < 4; i++) {
                    var $item_blog = '<div class="item-blog">' +
                        '<div class="post-img"><img src="' + data.items[i].featured_image + '"></div>' +
                        '<h2 class="title"><a target="_blank" href="' + data.items[i].link + '">' + data.items[i].title + '</a></h2></div>';
                    $('#content-blogMenu .col-recipesMenu').append($item_blog);
                };

                var $seeAll = 'Recipes<a target="_blank" href="' +  data.see_all_url + '">See all <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 492.004 492.004" style="enable-background:new 0 0 492.004 492.004;width: 11px;height: 11px;" xml:space="preserve"> <path d="M382.678,226.804L163.73,7.86C158.666,2.792,151.906,0,144.698,0s-13.968,2.792-19.032,7.86l-16.124,16.12    c-10.492,10.504-10.492,27.576,0,38.064L293.398,245.9l-184.06,184.06c-5.064,5.068-7.86,11.824-7.86,19.028    c0,7.212,2.796,13.968,7.86,19.04l16.124,16.116c5.068,5.068,11.824,7.86,19.032,7.86s13.968-2.792,19.032-7.86L382.678,265    c5.076-5.084,7.864-11.872,7.848-19.088C390.542,238.668,387.754,231.884,382.678,226.804z" style="&#10;    fill: #004936;&#10;"/></svg></a>';
                $('#content-blogMenu .col-recipesMenu .heading-blog').append($seeAll);

                var $seeMobile = '<a target="_blank" href="' +  data.see_all_url + '">Find delicious recipes <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="4px" height="7px" viewBox="0 0 4 7" version="1.1" <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square"> <g id="Muniq-menu-mobile-3" transform="translate(-269.000000, -575.000000)" stroke="#004936"> <g id="Group-4-Copy-6" transform="translate(269.500000, 578.500000) rotate(-45.000000) translate(-269.500000, -578.500000) translate(267.000000, 576.000000)"> <line x1="0.5" y1="4.5" x2="4.5" y2="4.5" id="Line-10"/> <line x1="4.5" y1="4.5" x2="4.5" y2="0.5" id="Line"/> </g> </g> </g> </svg></a>';
                $('#content-blogMenu-mobile .col-recipes-mobile .content-blog').append($seeMobile);
            }
        });

        // Ambassadors Menu
        $.ajax({
            url: "https://muniqwp.dev-directory.com/wp-json/api/latest-posts?category=ambassadors&limit=1",
            jsonp: "_jsonp",
            method: 'GET',
            dataType: 'jsonp',

            success: function(data) {

                var $item_blog = '<div class="item-blogAmbassador">' +
                    '<div class="post-ambassador"><img src="' + data.items[1].featured_image + '"></div>' +
                    '<h2 class="title"><a target="_blank" href="' + data.items[1].link + '">' + data.items[1].title + '</a></h2>' +
                    '<p>' + data.items[1].excerpt + '</p>' +
                    '</div>';
                $('#content-blogMenu .col-ambassadorMenu').append($item_blog);

                var $seeAll = 'Ambassador Stories<a target="_blank" href="' +  data.see_all_url + '">See all <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 492.004 492.004" style="enable-background:new 0 0 492.004 492.004;width: 11px;height: 11px;" xml:space="preserve"> <path d="M382.678,226.804L163.73,7.86C158.666,2.792,151.906,0,144.698,0s-13.968,2.792-19.032,7.86l-16.124,16.12    c-10.492,10.504-10.492,27.576,0,38.064L293.398,245.9l-184.06,184.06c-5.064,5.068-7.86,11.824-7.86,19.028    c0,7.212,2.796,13.968,7.86,19.04l16.124,16.116c5.068,5.068,11.824,7.86,19.032,7.86s13.968-2.792,19.032-7.86L382.678,265    c5.076-5.084,7.864-11.872,7.848-19.088C390.542,238.668,387.754,231.884,382.678,226.804z" style="&#10;    fill: #004936;&#10;"/></svg></a>';
                $('#content-blogMenu .col-ambassadorMenu .heading-blog').append($seeAll);

                var $seeMobile = '<a target="_blank" href="' +  data.see_all_url + '">Meet our ambassadors <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="4px" height="7px" viewBox="0 0 4 7" version="1.1" <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square"> <g id="Muniq-menu-mobile-3" transform="translate(-269.000000, -575.000000)" stroke="#004936"> <g id="Group-4-Copy-6" transform="translate(269.500000, 578.500000) rotate(-45.000000) translate(-269.500000, -578.500000) translate(267.000000, 576.000000)"> <line x1="0.5" y1="4.5" x2="4.5" y2="4.5" id="Line-10"/> <line x1="4.5" y1="4.5" x2="4.5" y2="0.5" id="Line"/> </g> </g> </g> </svg></a>';
                $('#content-blogMenu-mobile .col-ambassador-mobile .content-blog').append($seeMobile);
            }
        });
    });
}

function productJs() {

    $(document).ready(function(){
        $('body').addClass('loaded');
        $(document).on('click', '.flavor .form-option ', function () {
            if ($(this).hasClass('active') === false) {
                $('.form-option').removeClass('active');
                $(this).addClass('active');
                changeOption($(this));
            }
        });

        $('.flavor input').each(function () {
            if ($(this).prop('checked') === true) {
                var current = $(this).next();
                current.addClass('active');
                changeOption(current);
            }
        });

        //active slide product
        $('.productView-thumbnail:first-child').click();
        // active name product
        const name = $('.productView-title').text();
        $('.name-list-product li a').each(function () {
            if ($(this).text() == name) {
                $(this).parent().addClass('active');
                return false;
            }
        });
        // fix slide
        $(document).on('click', '.productView-image button', function () {
            const dataSlick = $('.productView-image--default.slick-active').attr('data-slick-index');
            $('.productView-thumbnail').each(function () {
                if ($(this).attr('data-slick-index') == dataSlick) {
                    $('.productView-thumbnail').removeClass('slick-active');
                    $(this).addClass('slick-active');
                }
            });
        });
        //drop down FAQ
        $('.question').click(function () {
            $(this).next().slideToggle();
            $(this).toggleClass('active');
        });
        // duplicate review
        $("#productReviews-list").appendTo(".review-container");
        // date review
        //$('.date-review').each(function () {
        //    const element = $(this).children('span'),
        //        date = $(this).attr('data-date');
        //    dateReview(date, element);
        //});

        // checkbox deliver
        $(document).on("click", ".box-deliver", function () {
            $('.box-deliver').parent().removeClass('active');
            $(this).parent().addClass('active');
            if ($(this).hasClass('one-time-box')) {
                $('.every-box').prop('checked', false);
                $('.deliver .option:nth-child(2) .form-option').click();
                $('.productView-price').addClass('active');
                $('#form-action-addToCart,.button-sticky .button').val('Add to cart')
            } else if ($(this).hasClass('every-box')) {
                $('.one-time-box').prop('checked', false);
                $('.deliver .option:nth-child(4) .form-option').click();
                $('.productView-price').removeClass('active');
                $('#form-action-addToCart,.button-sticky .button').val('Start now');
            }
        });
        $(document).on("click", ".button-sticky .button", function () {
            $('#form-action-addToCart').click();
        });
    });
}

function slide(optionActive) {
    const $image = $('.productView-image'),
        $thumb = $('.productView-thumbnails');


    if ($image.hasClass('slick-slider')) {
        $thumb.slick("unslick");
        $image.slick("unslick");
        const length = $image.children().length;
        $image.children().each(function () {
            const a = $(this).attr('alt');
            if (optionActive !== a) {
                $(this).remove();
            }
        });
        $thumb.children().each(function () {
            const a = $(this).attr('data-alt');
            if (optionActive !== a) {
                $(this).remove();
            }
        });
        $thumb.slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            arrows: false,
            focusOnSelect: true,
            asNavFor: '.productView-image'
        });
        $image.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            infinite: false,
            asNavFor: '.productView-thumbnails',
            responsive: [
                {
                    breakpoint: 980,
                    settings: {
                        dots: true
                    }
                }

            ]
        });
    } else {
        $thumb.slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            arrows: false,
            focusOnSelect: true,
            asNavFor: '.productView-image'
        });
        $image.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            infinite: false,
            asNavFor: '.productView-thumbnails',
            responsive: [
                {
                    breakpoint: 980,
                    settings: {
                        dots: true
                    }
                }

            ]
        });
    }
    $('.productView-thumbnail:first-child').click()
}

function changeOption(option) {
    const varriant = option.find('.form-option-variant').text().toLocaleLowerCase().replace(' ', '_'),
        optionActive = "[" + varriant + "]",
        content = $('.image-container');
    $('.productView-image').empty();
    $('.productView-thumbnails').empty();
    $('body').on('click', '.productView-thumbnail a', function (e) {
        e.preventDefault();
    });


    for (let i = 0; i < content.length; i++) {
        if (optionActive == content[i].attributes[1].value) {
            const a = content[i].children[0],
                b = content[i].children[1];
            $(a).clone().appendTo('.productView-image');
            $(b).clone().appendTo('.productView-thumbnails');
        }
    }
    $('.list-serving').removeClass('active');
    $('.list-field li').each(function () {
        const name = $(this).attr('data-name'),
            value = $(this).attr('data-value'),
            $title = $('.productView-title'),
            $description = $('.product-description p'),
            $calories = $('.calories'),
            $protein = $('.protein'),
            $sugar = $('.sugar'),
            $net_carbs = $('.net_carbs'),
            $vitamins_minerals = $('.vitamins_minerals'),
            $healthy_fats = $('.healthy_fats');
        if (name.includes(optionActive)) {
            if (name.includes("[title]")) {
                $title.html(value);
            }
            if (name.includes("[description]")) {
                $description.html(value);
            }
            if (name.includes("[calories]")) {
                if (value) {
                    $calories.html(value);
                    $calories.parent().addClass('active');
                }
            }
            if (name.includes("[protein]")) {
                if (value) {
                    $protein.html(value);
                    $protein.parent().addClass('active');
                }
            }
            if (name.includes("[sugar]")) {
                if (value) {
                    $sugar.html(value);
                    $sugar.parent().addClass('active');
                }
            }
            if (name.includes("[net_carbs]")) {
                if (value) {
                    $net_carbs.html(value);
                    $net_carbs.parent().addClass('active');
                }
            }
            if (name.includes("[vitamins_minerals]")) {
                if (value) {
                    $vitamins_minerals.html(value);
                    $vitamins_minerals.parent().addClass('active');
                }
            }
            if (name.includes("[healthy_fats]")) {
                if (value) {
                    $healthy_fats.html(value);
                    $healthy_fats.parent().addClass('active');
                }
            }
        }

    });
    slide(optionActive);
}

function timeDay(){

    const currentTime = new Date(),
          month = currentTime.getMonth() + 1,
          day = currentTime.getDate(),
          year = currentTime.getFullYear(),
          date1 =  month + "/" + day + "/" + year;

    $(".review-list .productReviews-list .productReview").each(function () {
        var date2 = $(this).find(".date-review").attr("data-date");

        var date1s = new Date(date1);
        var date2s = new Date(date2);

        var Difference_In_Time = date1s.getTime() - date2s.getTime();;
        var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);

        if(Difference_In_Days > 1){
            $(this).find(".date-review").append(Difference_In_Days + " days");
        } else {
            $(this).find(".date-review").append(Difference_In_Days + " day");
        }

    });

    if (window.ApplePaySession) {
        var host = window.location.hostname;
        var merchantIdentifier = host;
        var promise = ApplePaySession.canMakePaymentsWithActiveCard(merchantIdentifier);
        promise.then(function (canMakePayments) {
            if (canMakePayments){
                $(".cart-additionalCheckoutButtons .apple-pay-checkout-button").show();
                $(".previewCartCheckout .apple-pay-checkout-button").show();
            }
        });
    }
}
