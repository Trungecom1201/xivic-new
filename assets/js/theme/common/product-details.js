/*
 Import all product specific js
 */
import $ from 'jquery';
import utils from '@bigcommerce/stencil-utils';
import 'foundation-sites/js/foundation/foundation';
import 'foundation-sites/js/foundation/foundation.reveal';
import ImageGallery from '../product/image-gallery';
import modalFactory from '../global/modal';
import _ from 'lodash';



// We want to ensure that the events are bound to a single instance of the product details component
let productSingleton = null;

utils.hooks.on('cart-item-add', (event, form) => {
    if (productSingleton) {
        productSingleton.addProductToCart(event, form);
    }
});

utils.hooks.on('product-option-change', (event, changedOption) => {
    if (productSingleton) {
        productSingleton.productOptionsChanged(event, changedOption);
    }
});

export default class Product {
    constructor($scope, context, productAttributesData = {}) {
        this.$overlay = $('[data-cart-item-add] .loadingOverlay');
        this.$scope = $scope;
        this.context = context;
        this.imageGallery = new ImageGallery($('[data-image-gallery]', this.$scope));
        this.imageGallery.init();
        this.listenQuantityChange();
        this.initRadioAttributes();
        //this.slideNatural();
        const dataPrice = $('.quantity input:checked').next().children().html(),
            priceTax = $('meta[itemprop="price"]').attr('content');
        this.calculatorPrice(dataPrice,priceTax);
        //this.productJs();
        const $form = $('form[data-cart-item-add]', $scope);
        const $productOptionsElement = $('[data-product-option-change]', $form);
        const hasOptions = $productOptionsElement.html().trim().length;

        // Update product attributes. If we're in quick view and the product has options, then also update the initial view in case items are oos
        if (_.isEmpty(productAttributesData) && hasOptions) {
            const $productId = $('[name="product_id"]', $form).val();

            utils.api.productAttributes.optionChange($productId, $form.serialize(), (err, response) => {
                const attributesData = response.data || {};

                this.updateProductAttributes(attributesData);
                this.updateView(attributesData);
            });
        } else {
            this.updateProductAttributes(productAttributesData);
        }

        $productOptionsElement.show();

        this.previewModal = modalFactory('#previewModal')[0];
        productSingleton = this;
    }

    /**
     * Since $productView can be dynamically inserted using render_with,
     * We have to retrieve the respective elements
     *
     * @param $scope
     */
    getViewModel($scope) {
        return {
            $priceWithTax: $('[data-product-price-with-tax]', $scope),
            $rrpWithTax: $('[data-product-rrp-with-tax]', $scope),
            $priceWithoutTax: $('[data-product-price-without-tax]', $scope),
            $rrpWithoutTax: $('[data-product-rrp-without-tax]', $scope),
            $weight: $('.productView-info [data-product-weight]', $scope),
            $increments: $('.form-field--increments :input', $scope),
            $addToCart: $('#form-action-addToCart', $scope),
            $wishlistVariation: $('[data-wishlist-add] [name="variation_id"]', $scope),
            stock: {
                $container: $('.form-field--stock', $scope),
                $input: $('[data-product-stock]', $scope),
            },
            $sku: $('[data-product-sku]'),
            quantity: {
                $text: $('.incrementTotal', $scope),
                $input: $('[name=qty\\[\\]]', $scope),
            },
        };
    }

    /**
     * Checks if the current window is being run inside an iframe
     * @returns {boolean}
     */
    isRunningInIframe() {
        try {
            return window.self !== window.top;
        } catch (e) {
            return true;
        }
    }

    /**
     *
     * Handle product options changes
     *
     */
    productOptionsChanged(event, changedOption) {
        const $changedOption = $(changedOption);
        const $form = $changedOption.parents('form');
        const productId = $('[name="product_id"]', $form).val();

        // Do not trigger an ajax request if it's a file or if the browser doesn't support FormData
        if ($changedOption.attr('type') === 'file' || window.FormData === undefined) {
            return;
        }

        utils.api.productAttributes.optionChange(productId, $form.serialize(), (err, response) => {
            const productAttributesData = response.data || {};

            this.updateProductAttributes(productAttributesData);
            this.updateView(productAttributesData);
        });
    }

    showProductImage(image) {
        if (_.isPlainObject(image)) {
            const zoomImageUrl = utils.tools.image.getSrc(
                image.data,
                this.context.themeSettings.zoom_size
            );

            const mainImageUrl = utils.tools.image.getSrc(
                image.data,
                this.context.themeSettings.product_size
            );

            this.imageGallery.setAlternateImage({
                mainImageUrl,
                zoomImageUrl,
            });
        } else {
            this.imageGallery.restoreImage();
        }
    }

    /**
     *
     * Handle action when the shopper clicks on + / - for quantity
     *
     */
    listenQuantityChange() {
        this.$scope.on('click', '[data-quantity-change] button', (event) => {
            event.preventDefault();
            const $target = $(event.currentTarget);
            const viewModel = this.getViewModel(this.$scope);
            const $input = viewModel.quantity.$input;
            const quantityMin = parseInt($input.data('quantity-min'), 10);
            const quantityMax = parseInt($input.data('quantity-max'), 10);

            let qty = parseInt($input.val(), 10);

            // If action is incrementing
            if ($target.data('action') === 'inc') {
                // If quantity max option is set
                if (quantityMax > 0) {
                    // Check quantity does not exceed max
                    if ((qty + 1) <= quantityMax) {
                        qty++;
                    }
                } else {
                    qty++;
                }
            } else if (qty > 1) {
                // If quantity min option is set
                if (quantityMin > 0) {
                    // Check quantity does not fall below min
                    if ((qty - 1) >= quantityMin) {
                        qty--;
                    }
                } else {
                    qty--;
                }
            }

            // update hidden input
            viewModel.quantity.$input.val(qty);
            // update text
            viewModel.quantity.$text.text(qty);
        });
    }

    /**
     *
     * Add a product to cart
     *
     */
    addProductToCart(event, form) {
        const $addToCartBtn = $('#form-action-addToCart', $(event.target));
        const originalBtnVal = $addToCartBtn.val();
        const waitMessage = $addToCartBtn.data('waitMessage');

        // Do not do AJAX if browser doesn't support FormData
        if (window.FormData === undefined) {
            return;
        }

        // Prevent default
        event.preventDefault();

        $addToCartBtn
            .val(waitMessage)
            .prop('disabled', true);

        this.$overlay.show();

        // Add item to cart
        utils.api.cart.itemAdd(new FormData(form), (err, response) => {
            const errorMessage = err || response.data.error;

            $addToCartBtn
                .val(originalBtnVal)
                .prop('disabled', false);

            this.$overlay.hide();

            // Guard statement
            if (errorMessage) {
                // Strip the HTML from the error message
                const tmp = document.createElement('DIV');
                tmp.innerHTML = errorMessage;

                return alert(tmp.textContent || tmp.innerText);
            }

            // Open preview modal and update content
            if (this.previewModal) {
                this.previewModal.open();

                this.updateCartContent(this.previewModal, response.data.cart_item.hash);
            } else {
                this.$overlay.show();
                // if no modal, redirect to the cart page
                this.redirectTo(response.data.cart_item.cart_url || this.context.urls.cart);
            }
        });
    }

    /**
     * Get cart contents
     *
     * @param {String} cartItemHash
     * @param {Function} onComplete
     */
    getCartContent(cartItemHash, onComplete) {
        const options = {
            template: 'cart/preview',
            params: {
                suggest: cartItemHash,
            },
            config: {
                cart: {
                    suggestions: {
                        limit: 4,
                    },
                },
            },
        };

        utils.api.cart.getContent(options, onComplete);
    }

    /**
     * Redirect to url
     *
     * @param {String} url
     */
    redirectTo(url) {
        if (this.isRunningInIframe() && !window.iframeSdk) {
            window.top.location = url;
        } else {
            window.location = url;
        }
    }

    /**
     * Update cart content
     *
     * @param {Modal} modal
     * @param {String} cartItemHash
     * @param {Function} onComplete
     */
    updateCartContent(modal, cartItemHash, onComplete) {
        this.getCartContent(cartItemHash, (err, response) => {
            if (err) {
                return;
            }

            modal.updateContent(response);

            // Update cart counter
            const $body = $('body');
            const $cartQuantity = $('[data-cart-quantity]', modal.$content);
            const $cartCounter = $('.navUser-action .cart-count');
            const quantity = $cartQuantity.data('cart-quantity') || 0;

            $cartCounter.addClass('cart-count--positive');
            $body.trigger('cart-quantity-update', quantity);

            if (onComplete) {
                onComplete(response);
            }
        });
    }

    /**
     * Show an message box if a message is passed
     * Hide the box if the message is empty
     * @param  {String} message
     */
    showMessageBox(message) {
        const $messageBox = $('.productAttributes-message');

        if (message) {
            $('.alertBox-message', $messageBox).text(message);
            $messageBox.show();
        } else {
            $messageBox.hide();
        }
    }

    /**
     * Update the view of price, messages, SKU and stock options when a product option changes
     * @param  {Object} data Product attribute data
     */
    updatePriceView(viewModel, price) {
        if (price.with_tax) {
            viewModel.$priceWithTax.html(price.with_tax.formatted);
        }

        if (price.without_tax) {
            viewModel.$priceWithoutTax.html(price.without_tax.formatted);
        }

        if (price.rrp_with_tax) {
            viewModel.$rrpWithTax.html(price.rrp_with_tax.formatted);
        }

        if (price.rrp_without_tax) {
            viewModel.$rrpWithoutTax.html(price.rrp_without_tax.formatted);
        }
    }

    /**
     * Update the view of price, messages, SKU and stock options when a product option changes
     * @param  {Object} data Product attribute data
     */
    updateView(data) {
        const viewModel = this.getViewModel(this.$scope);
        this.showMessageBox(data.stock_message || data.purchasing_message);

        if (_.isObject(data.price)) {
            this.updatePriceView(viewModel, data.price);
            const dataPrice = $('.quantity input:checked').next().children().html();
            this.calculatorPrice(dataPrice,data.price.without_tax.value);
        }

        if (_.isObject(data.weight)) {
            viewModel.$weight.html(data.weight.formatted);
        }

        // Set variation_id if it exists for adding to wishlist
        if (data.variantId) {
            viewModel.$wishlistVariation.val(data.variantId);
        }

        // If SKU is available
        if (data.sku) {
            viewModel.$sku.text(data.sku);
        }

        // if stock view is on (CP settings)
        if (viewModel.stock.$container.length && _.isNumber(data.stock)) {
            // if the stock container is hidden, show
            viewModel.stock.$container.removeClass('u-hiddenVisually');

            viewModel.stock.$input.text(data.stock);
        }

        if (!data.purchasable || !data.instock) {
            viewModel.$addToCart.prop('disabled', true);
            viewModel.$increments.prop('disabled', true);
        } else {
            viewModel.$addToCart.prop('disabled', false);
            viewModel.$increments.prop('disabled', false);
        }
    }

    /**
     * Hide or mark as unavailable out of stock attributes if enabled
     * @param  {Object} data Product attribute data
     */
    updateProductAttributes(data) {
        const behavior = data.out_of_stock_behavior;
        const inStockIds = data.in_stock_attributes;
        const outOfStockMessage = ` (${data.out_of_stock_message})`;

        this.showProductImage(data.image);

        if (behavior !== 'hide_option' && behavior !== 'label_option') {
            return;
        }

        $('[data-product-attribute-value]', this.$scope).each((i, attribute) => {
            const $attribute = $(attribute);
            const attrId = parseInt($attribute.data('product-attribute-value'), 10);


            if (inStockIds.indexOf(attrId) !== -1) {
                this.enableAttribute($attribute, behavior, outOfStockMessage);
            } else {
                this.disableAttribute($attribute, behavior, outOfStockMessage);
            }
        });
    }

    disableAttribute($attribute, behavior, outOfStockMessage) {
        if (this.getAttributeType($attribute) === 'set-select') {
            return this.disableSelectOptionAttribute($attribute, behavior, outOfStockMessage);
        }

        if (behavior === 'hide_option') {
            $attribute.hide();
        } else {
            $attribute.addClass('unavailable');
        }
    }

    disableSelectOptionAttribute($attribute, behavior, outOfStockMessage) {
        const $select = $attribute.parent();

        if (behavior === 'hide_option') {
            $attribute.toggleOption(false);
            // If the attribute is the selected option in a select dropdown, select the first option (MERC-639)
            if ($attribute.parent().val() === $attribute.attr('value')) {
                $select[0].selectedIndex = 0;
            }
        } else {
            $attribute.attr('disabled', 'disabled');
            $attribute.html($attribute.html().replace(outOfStockMessage, '') + outOfStockMessage);
        }
    }

    enableAttribute($attribute, behavior, outOfStockMessage) {
        if (this.getAttributeType($attribute) === 'set-select') {
            return this.enableSelectOptionAttribute($attribute, behavior, outOfStockMessage);
        }

        if (behavior === 'hide_option') {
            $attribute.show();
        } else {
            $attribute.removeClass('unavailable');
        }
    }

    enableSelectOptionAttribute($attribute, behavior, outOfStockMessage) {
        if (behavior === 'hide_option') {
            $attribute.toggleOption(true);
        } else {
            $attribute.removeAttr('disabled');
            $attribute.html($attribute.html().replace(outOfStockMessage, ''));
        }
    }

    getAttributeType($attribute) {
        const $parent = $attribute.closest('[data-product-attribute]');

        return $parent ? $parent.data('product-attribute') : null;
    }

    /**
     * Allow radio buttons to get deselected
     */
    initRadioAttributes() {
        $('[data-product-attribute] input[type="radio"]', this.$scope).each((i, radio) => {
            const $radio = $(radio);

            // Only bind to click once
            if ($radio.attr('data-state') !== undefined) {
                $radio.click(() => {
                    if ($radio.data('state') === true) {
                        $radio.prop('checked', false);
                        $radio.data('state', false);

                        $radio.change();
                    } else {
                        $radio.data('state', true);
                    }

                    this.initRadioAttributes();
                });
            }

            $radio.attr('data-state', $radio.prop('checked'));
        });
    }
    calculatorPrice(data,discountPrice){
        const defaultPrice = $('meta[property="product:price:amount"]').attr('content'),
            realPrice = (data/14) * defaultPrice,
            discountPercentage = Math.floor(100 - (discountPrice * 100)/realPrice);
        $('.real-price').html("$"+realPrice);
        $('.save-percent').html("(save " + discountPercentage + "%)");
    }


    //productJs(){
    //    $('body').addClass('loaded');
    //    $(document).on('click','.flavor .form-option ',function () {
    //        if($(this).hasClass('active') === false){
    //            $('.form-option').removeClass('active');
    //            $(this).addClass('active');
    //            changeOption($(this));
    //        }
    //    });
    //    $('.flavor input').each(function () {
    //        if($(this).prop('checked') === true){
    //            var current = $(this).next();
    //            current.addClass('active');
    //            changeOption(current);
    //        }
    //    });
    //    function slide(optionActive) {
    //        const $image =$('.productView-image'),
    //            $thumb = $('.productView-thumbnails');
    //
    //
    //        if ($image.hasClass('slick-slider')){
    //            $thumb.slick("unslick");
    //            $image.slick("unslick");
    //            const length = $image.children().length;
    //            $image.children().each(function () {
    //                const a = $(this).attr('alt');
    //                if(optionActive !== a){
    //                    $(this).remove();
    //                }
    //            });
    //            $thumb.children().each(function () {
    //                const a = $(this).attr('data-alt');
    //                if(optionActive !== a){
    //                    $(this).remove();
    //                }
    //            });
    //            $thumb.slick({
    //                slidesToShow: 5,
    //                slidesToScroll: 1,
    //                arrows: false,
    //                focusOnSelect: true,
    //                asNavFor: '.productView-image'
    //            });
    //            $image.slick({
    //                slidesToShow: 1,
    //                slidesToScroll: 1,
    //                arrows: true,
    //                infinite: false,
    //                asNavFor: '.productView-thumbnails',
    //                responsive: [
    //                    {
    //                        breakpoint: 980,
    //                        settings: {
    //                            dots:true
    //                        }
    //                    }
    //
    //                ]
    //            });
    //        }else{
    //            $thumb.slick({
    //                slidesToShow: 5,
    //                slidesToScroll: 1,
    //                arrows: false,
    //                focusOnSelect: true,
    //                asNavFor: '.productView-image'
    //            });
    //            $image.slick({
    //                slidesToShow: 1,
    //                slidesToScroll: 1,
    //                arrows: true,
    //                infinite: false,
    //                asNavFor: '.productView-thumbnails',
    //                responsive: [
    //                    {
    //                        breakpoint: 980,
    //                        settings: {
    //                            dots:true
    //                        }
    //                    }
    //
    //                ]
    //            });
    //        }
    //        $('.productView-thumbnail:first-child').click()
    //    }
    //    function changeOption(option){
    //        const varriant = option.find('.form-option-variant').text().toLocaleLowerCase().replace(' ','_'),
    //            optionActive = "["+varriant+"]",
    //            content = $('.image-container');
    //        $('.productView-image').empty();
    //        $('.productView-thumbnails').empty();
    //        $('body').on('click','.productView-thumbnail a',function (e) {
    //            e.preventDefault();
    //        });
    //
    //
    //        for(let i = 0 ; i < content.length; i++){
    //            if(optionActive == content[i].attributes[1].value){
    //                const a = content[i].children[0],
    //                    b = content[i].children[1];
    //                $(a).clone().appendTo('.productView-image');
    //                $(b).clone().appendTo('.productView-thumbnails');
    //            }
    //        }
    //        $('.list-serving').removeClass('active');
    //        $('.list-field li').each(function () {
    //            const name = $(this).attr('data-name'),
    //                value = $(this).attr('data-value'),
    //                $title = $('.productView-title'),
    //                $description = $('.product-description p'),
    //                $calories = $('.calories'),
    //                $protein = $('.protein'),
    //                $sugar = $('.sugar'),
    //                $net_carbs =$('.net_carbs'),
    //                $vitamins_minerals =  $('.vitamins_minerals'),
    //                $healthy_fats =$('.healthy_fats');
    //            if(name.includes(optionActive)){
    //                if (name.includes("[title]")){
    //                    $title.html(value);
    //                }
    //                if(name.includes("[description]")){
    //                    $description.html(value);
    //                }
    //                if (name.includes("[calories]")){
    //                    if (value){
    //                        $calories.html(value);
    //                        $calories.parent().addClass('active');
    //                    }
    //                }
    //                if (name.includes("[protein]")) {
    //                    if (value) {
    //                        $protein.html(value);
    //                        $protein.parent().addClass('active');
    //                    }
    //                }
    //                if (name.includes("[sugar]")){
    //                    if (value) {
    //                        $sugar.html(value);
    //                        $sugar.parent().addClass('active');
    //                    }
    //                }
    //                if (name.includes("[net_carbs]")){
    //                    if (value) {
    //                        $net_carbs.html(value);
    //                        $net_carbs.parent().addClass('active');
    //                    }
    //                }
    //                if (name.includes("[vitamins_minerals]")){
    //                    if (value) {
    //                        $vitamins_minerals.html(value);
    //                        $vitamins_minerals.parent().addClass('active');
    //                    }
    //                }
    //                if (name.includes("[healthy_fats]")){
    //                    if (value) {
    //                        $healthy_fats.html(value);
    //                        $healthy_fats.parent().addClass('active');
    //                    }
    //                }
    //            }
    //
    //        });
    //        slide(optionActive);
    //    }
    //    //active slide product
    //    $('.productView-thumbnail:first-child').click();
    //    // active name product
    //    const name =  $('.productView-title').text();
    //    $('.name-list-product li a').each(function () {
    //        if($(this).text() == name){
    //            $(this).parent().addClass('active');
    //            return false;
    //        }
    //    });
    //    // fix slide
    //    $(document).on('click','.productView-image button',function () {
    //        const dataSlick = $('.productView-image--default.slick-active').attr('data-slick-index');
    //        $('.productView-thumbnail').each(function () {
    //            if($(this).attr('data-slick-index') == dataSlick){
    //                $('.productView-thumbnail').removeClass('slick-active');
    //                $(this).addClass('slick-active');
    //            }
    //        });
    //    });
    //    //drop down FAQ
    //    $('.question').click(function () {
    //        $(this).next().slideToggle();
    //        $(this).toggleClass('active');
    //    });
    //    // duplicate review
    //    $( "#productReviews-list" ).appendTo( ".review-container" );
    //    // date review
    //    $('.date-review').each(function () {
    //        const element = $(this).children('span'),
    //            date = $(this).attr('data-date');
    //        dateReview(date,element);
    //    });
    //    function dateReview(date,element) {
    //        const date2 = new Date(date);
    //        const date1 = new Date();
    //        const diffTime = Math.abs(date2 - date1);
    //        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    //        element.html(diffDays);
    //    }
    //    // checkbox deliver
    //    $(document).on("click", ".box-deliver", function () {
    //        $('.box-deliver').parent().removeClass('active');
    //        $(this).parent().addClass('active');
    //        if($(this).hasClass('one-time-box')){
    //            $('.every-box').prop('checked',false);
    //            $('.deliver .option:nth-child(2) .form-option').click();
    //            $('.productView-price').addClass('active');
    //            $('#form-action-addToCart,.button-sticky .button').val('Add to cart')
    //        }else if($(this).hasClass('every-box')){
    //            $('.one-time-box').prop('checked',false);
    //            $('.deliver .option:nth-child(4) .form-option').click();
    //            $('.productView-price').removeClass('active');
    //            $('#form-action-addToCart,.button-sticky .button').val('Start now');
    //        }
    //    });
    //    $(document).on("click", ".button-sticky .button", function () {
    //        $('#form-action-addToCart').click();
    //    });
    //}
}
